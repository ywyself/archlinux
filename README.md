# ArchLinux 安装，UEFI+GPT
## 安装前

### 设置键盘布局
> 可以省略，默认为us键盘布局
- 查看可用的键盘布局
    ```
    # ls /usr/share/kbd/keymaps/**/*.map.gz
    ```    
- 设置键盘布局
    ```
    # loadkeys us
    ```
### 验证启动方式

> 以下命令执行成功则为uefi模式启动
- 执行命令
    ```
     # ls /sys/firmware/efi/efivars
     ```

### 连接网络
> 最好使用有线连接，因为简单
- 测试网络是否已经连接
    ```
    # ping www.baidu.com
    ```
1. 使用有线连接网络
    ```
    # dhcpcd
    ```
2. 使用无线连接
    > 使用`wifi-menu`命令，命令可能无法使用，需要先安装`wpa_supplicant`和`dialog`模块
    ```
    # wifi-menu
    ```
    > 安装`wpa_supplicant`
    ```
    # pacman -S wpa_supplicant
    ```
### 更新系统时间
- 设置时间同步
    > 注意用`date`看一下时间，与正确时间差了8个小时
    ```
    # timedatectl set-ntp true
    ```
### 对磁盘进行分区
> 目前划分的是3个分区，`/boot`，`/home`,`/`
- 对磁盘进行分区
    1. 使用`parted`进行磁盘分区
        - 查看磁盘设备
            ```
            # parted -l
            ```
            > 以下为虚拟机上的分区，分区表格式为mbr
            > ```
            > Model: Virtio Block Device (virtblk)
            > Disk /dev/vda: 42.9GB
            > Sector size (logical/physical): 512B/512B
            > Partition Table: msdos
            > Disk Flags:
            >
            > Number Start   End     Size    Type     File system  Flags
            > 1      1049kB  42.9GB  42.9GB  primary  ext4         boot
            ```
        - 选择操作设备块
            ```
            # parted /dev/vda
            ```
        - 修改为`GPT`格式
            ```
            (parted) mklabel
            New disk label type? gpt                                                  
            Warning: Partition(s) on /dev/vda are being used.
            Ignore/Cancel? i                                                          
            Warning: The existing disk label on /dev/vda will be destroyed and all data on this disk will be lost. Do you want to continue?
            Yes/No? y
            ```
        - 删除已有分区
            > 使用rm 数字删除分区
            ```
            (parted) rm 1
            ```
        - 新建分区
            - 新建`boot`分区
                ```
                (parted) mkpart
                Partition name?  []?
                File system type?  [ext2]? ext4
                Start? 0
                End? 512M
                ```
            - 新建`home`分区
                ```
                (parted) mkpart
                Partition name?  []? /home
                File system type?  [ext2]? ext4
                Start? 512M
                End? 10G
                ```
            - 新建`根`分区
                ```
                (parted) mkpart
                Partition name?  []? /
                File system type?  [ext2]?
                Start? 10G
                End? -1
                ```
            - 查看分区信息
                ```
                (parted) p
                ```
                > ```
                > Model: Virtio Block Device (virtblk)
                > Disk /dev/vda: 42.9GB
                > Sector size (logical/physical): 512B/512B
                > Partition Table: gpt
                > Disk Flags:
                >
                > Number  Start   End     Size    File system  Name   Flags
                >  1      17.4kB  512MB   512MB
                >  2      512MB   10.0GB  9488MB               /home
                >  3      10.0GB  42.9GB  32.9GB               /
                > ```
        - 退出
            ```
            (parted) quit
            ```
    2. 使用`fdisk`进行磁盘分区
        > 进行`fdisk`的命令交互操作时候不能删除输入的内容
        - 选择操作设备块
            ```
            # fdisk /dev/vda
            WARNING: fdisk GPT support is currently new, and therefore in an experimental phase. Use at your own discretion.
            Welcome to fdisk (util-linux 2.23.2).

            Changes will remain in memory only, until you decide to write them.
            Be careful before using the write command.


            Command (m for help):
            ```
        - 创建一个`empty gpt`分区表
            ```
            Command (m for help): g
            Building a new GPT disklabel (GUID: C5734B4C-B436-466C-9D1C-693388EA850A)
            ```
        - 创建分区，作为`boot`分区
            ```
            Command (m for help): n
            Partition number (1-128, default 1):
            First sector (2048-83886046, default 2048):
            Last sector, +sectors or +size{K,M,G,T,P} (2048-83886046, default 83886046): +512M
            Created partition 1
            ```
        - 创建分区，作为`home`分区
            ```
            Command (m for help): n
            Partition number (2-128, default 2):
            First sector (1050624-83886046, default 1050624):
            Last sector, +sectors or +size{K,M,G,T,P} (1050624-83886046, default 83886046): +10G
            Created partition 2
            ```
        - 创建分区，作为`/`分区
            ```
            Command (m for help): n
            Partition number (3-128, default 3):
            First sector (22022144-83886046, default 22022144):
            Last sector, +sectors or +size{K,M,G,T,P} (22022144-83886046, default 83886046):
            Created partition 3
            ```
        - 查看分区信息
            ```
            Command (m for help): p
            ```
            > ```
            > Disk /dev/vda: 42.9 GB, 42949672960 bytes, 83886080 sectors
            > Units = sectors of 1 * 512 = 512 bytes
            > Sector size (logical/physical): 512 bytes / 512 bytes
            > I/O size (minimum/optimal): 512 bytes / 512 bytes
            > Disk label type: gpt
            >
            >
            > #         Start          End    Size  Type            Name
            > 1           34      1000000  488.3M  Microsoft basic
            > 2      1000001     19531250    8.9G  Microsoft basic /home
            > 3     19531776     83884031   30.7G  Microsoft basic /
            ```
        - 格式化`boot`分区
            > 必须为fat32格式
            ```
            # mkfs.fat -F32 /dev/sdxY
            ```
        - 格式化`home`,`/`分区
            ```
            # mkfs.ext4 /dev/sdxY
            ```
### 挂载分区
- 挂载`/`分区
    ```
    # mount /dev/sda3 /mnt
    ```
- 挂载`boot`,`home`分区
    > 一定要先挂载`/`分区，不然这两个挂载点会被覆盖
    ```
    # mkdir /mnt/boot
    # mount /dev/sda1 /mnt/boot
    # mkdir /mnt/home
    # mount /dev/sda1 /mnt/home
    ```
## 安装
### 选择镜像源地址
> 这一步可以跳过，安装过程中网速可能有些慢
- 安装`vim`
    > vim最好装上
    ```
    # pacman -S vim
    ```
- 修改镜像源地址
    ```
    # vim /etc/pacman.d/mirrorlist
    ```
### 安装基础包
> 也可以使用`# pacstrap /mnt base base-devel`
```
# pacstrap /mnt base
```
## 配置系统
### 生成挂载文件（开机自动挂载分区）
```
# genfstab -U /mnt >> /mnt/etc/fstab
```
> 生成之后检查一下文件内容，应该有3个分区被挂载
### 切换到新系统中
```
# arch-chroot /mnt
```
### 设置本地时区
- 设置时区
    > 再看时间是对的
    ```
    # ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
    ```
- 同步时间到硬件时钟
    ```
    # hwclock --systohc
    ```
### 本地化
- 取消`/etc/locale.gen`文件中下面行的注释
    > ```
    > zh_CN.UTF-8 UTF-8
    > zh_HK.UTF-8 UTF-8
    > zh_TW.UTF-8 UTF-8
    > en_US.UTF-8 UTF-8
    > ```
- 执行命令
    ```
    # locale-gen
    ```
- 设置本地语言
    - 打开`/etc/locale.conf`文件
        ```
        # vim /etc/locale.conf
        ```
    - 加入以下内容
        > 设置为英文，目前还没有安装中文字体
        ```
        LANG=en_US.UTF-8
        ```
### 设置主机名
> 将`/etc/hostname`文件的内容修改为`archlinux`
```
# vim /etc/hostname
```
### 修改`hosts`文件
> 添加以下内容到`/etc/hosts`
> ```
> 127.0.0.1	localhost
> ::1		    localhost
> ```
```
# vim /etc/hosts
```
### 网络配置
- 安装`dialog`, `wpa_supplicant`
    > 安装`wpa_supplicant`后可以使用`wifi-menu`命令，用界面连接wifi，依赖`dialog`模块
    ```
    # pacman -S wpa_supplicant dialog
    ```
### 为`root`用户设置密码
> 使用命令，然后根据提示输入密码
```
# passwd
```
### 安装Intel-ucode
> 非IntelCPU可以跳过此步骤
```
# pacman -S intel-ucode
```
### 安装启动引导
- 安装`grub`与`efibootmgr`
    ```
    # pacman -S grub efibootmgr
    ```
- 部署`grub`
    ```
    # grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
    ```
- 生成配置文件
    ```
    # grub-mkconfig -o /boot/grub/grub.cfg
    ```
- 最后检查一下生成的文件
    > 看一下Arch Linux入口是否出现在文件中
    ```
    # vim /boot/grub/grub.cfg
    ```
## 安装完成
- 退出新系统
    > 这个时候应该是在新系统中。关于如何区分是在新系统还是在安装盘中，可以看命令提示符前面的`root`字的颜色，彩色的表示在安装盘中
    ```
    # exit
    ```
- 重启
    ```
    # reboot
    ```

## 继续一些设置
> 重启后进入新系统，到了字符界面，继续一些其他的设置，以及桌面环境的安装
### 创建交换文件
- 分配一块空间用于交换文件
    ```
    # fallocate -l 512M /swapfile
    ```
- 更改权限
    > 600 = rw- --- ---
    ```
    # chmod 600 /swapfile
    ```
- 设置交换文件
    ```
    # mkswap /swapfile
    ```
- 启用交换文件
    ```
    # swapon /swapfile
    ```
- 添加交换文件到`/etc/fstab`
    ```
    # vim /etc/fstab
    ```
    > 新增加一行
    > ```
    > /swapfile none swap defaults 0 0
    > ```
### 新建一个用户
- 新建用户
    ```
    # useradd -m -G wheel myUserName
    ```
    > - -m 是为用户创建家目录(如果有则不用这个参数)
    > - -G 为用户设置一个分组，wheel属于管理组
- 给新用户设置密码
    ```
    # passwd myUserName
    ```
### 配置sudo
> 如果在安装系统时安装了`base-devel`这个包，那么这个命令在安装系统是一起安装了
- 安装`sudo`
    ```
    # pacman -S sudo
    ```
- 修改`/etc/sudoers`
    - 方法一：在`root ALL=(ALL) ALL`行下新增加一行
        ```
        myUserName  ALL=(ALL)   ALL
        ```
    - 方法二：取消`%wheel  ALL=(ALL)       ALL`行的注释
### 文件系统支持
> 要支持制作fat文件系统，安装dosfstools，默认内核只能读取ntfs，要支持ntfs读写，安装ntfs-3g
```
# pacman -S ntfs-3g dosfstools
```

## 安装桌面环境
### 连接网络
- 连接网络
    - 有线使用`dhcpcd`
    - 无线使用`wifi-menu`
- 检测一下
    ```
    # ping www.baidu.com
    ```
### 安装驱动
- 安装显卡驱动
    > 在安装Xorg的时候好像会一起安装
    ```
    # pacman -S xf86-video-intel
    ```
### 安装桌面
- 安装Xorg服务
    ```
    # pacman -S xorg
    ```
- 安装桌面Xfce
    ```
    # pacman -S xfce4 xfce4-goodies
    ```
- 安装桌面管理器sddm
    ```
    # pacman -S sddm
    ```
- 设置开机启动sddm服务
    ```
    # systemctl enable sddm
    ```
### 提前配置网络
- 安装`networkmanager`软件
    > 是图形化的网络管理工具，安装桌面后需要用到
    ```
    # pacman -S networkmanager
    ```
- 启用`NetworkManager`网络服务
    ```
    # systemctl disable netctl
    # systemctl enable NetworkManager
    ```
- 安装工具栏工具来显示网络设置图标
    ```
    # pacman -S network-manager-applet
    ```
### 字体设置
- 安装字体
    ```
    # pacman -S ttf-roboto noto-fonts noto-fonts-cjk adobe-source-han-sans-cn-fonts adobe-source-han-serif-cn-fonts ttf-dejavu
    ```
- 开启桌面（系统）中文
    > 开启中文后字符界面（tty）无法显示中文，无法进入桌面时可以将系统字体改为英文，通常也没有机会再用tty了
    - 修改`/etc/locale.conf`
        > 将
        > ```
        > LANG=en_US.UTF-8
        > ```
        > 改为
        > ```
        > LANG=zh_CN.UTF-8
        > ```
### 安装输入法
- 安装`fcitx`
    ```
    # pacman -S fcitx
    ```
- 安装输入法模块
    ```
    # pacman -S fcitx-gtk3
    ```
- 安装一个输入法
    ```
    # pacman -S fcitx-libpinyin
    ```
- 安装输入法图形设置界面
    ```
    # pacman -S fcitx-configtool
    ```

## 安装一些软件
### chromuim
> 浏览器
```
# pacman -S chromuim
```
### screenfetch
> 在终端打印发行版logo
```
# pacman -S screenfetch
```
### dock
> 要获得像苹果osx一样的dock可以用docky或者dash-to-dock
```
# pacman -S docky
```
### sl
> 火车头
```
# pacman -S sl
```


